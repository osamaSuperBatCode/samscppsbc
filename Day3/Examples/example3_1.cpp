#include <iostream>

int main()
{
  using std::cout;

  cout << "The Size of an int is:\t\t" << sizeof(int) << " bytes.\n";
  cout << "The Size of an short is:\t\t" << sizeof(short) << " bytes.\n";
  cout << "The Size of an long is:\t\t" << sizeof(long) << " bytes.\n";
  cout << "The Size of an char is:\t\t" << sizeof(char) << " bytes.\n";
  cout << "The Size of an float is:\t\t" << sizeof(float) << " bytes.\n";
  cout << "The Size of an double is:\t\t" << sizeof(double) << " bytes.\n";
  cout << "The Size of an bool is:\t\t" << sizeof(bool) << " bytes.\n";
}
