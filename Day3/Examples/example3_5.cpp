#include <iostream>
int main()
{
  using std::cout;
  using std::endl;

  signed short int smallNumber;
  smallNumber = 32767;
  cout << "small number: " << smallNumber << endl;
  smallNumber++;
  cout << "small number: " << smallNumber << endl;
  smallNumber++;
  cout << "smallNumber: " << smallNumber << endl;
  return 0;
}
